using UnityEngine;
using System.Collections;


[RequireComponent(typeof(MMT.MobileMovieTexture))]
public class TestMobileTexture : MonoBehaviour 
{
    private MMT.MobileMovieTexture m_movieTexture;

    void Awake()
    {
        m_movieTexture = GetComponent<MMT.MobileMovieTexture>();

        m_movieTexture.onFinished += OnFinished;
    }

    void OnFinished(MMT.MobileMovieTexture sender)
    {
        Debug.Log(sender.Path + " has finished ");
    }

    float seekPosition;

    private void OnGUI()
    {
        GUILayout.BeginArea(new Rect(0.0f, 0.0f, Screen.width, Screen.height));

        var currentPosition = (float)m_movieTexture.playPosition;

        var newPosition = GUILayout.HorizontalSlider(currentPosition,0.0f,(float)m_movieTexture.duration);

        if (newPosition != currentPosition)
        {
            m_movieTexture.playPosition = newPosition;
        }
        

        //var newPosition = GUILayout.HorizontalSlider(currentPosition, )(currentPosition, 1.0f, 0.0f, (float)m_movieTexture.duration);

        //if (newPosition != currentPosition && GUIUtility.hotControl == 0)
        //{
        //    m_movieTexture.playPosition = newPosition;
        //}
        
        //seekPosition = (float)m_movieTexture.playPosition;
        //}
        //else
        //{
        //    seekPosition = (float)m_movieTexture.playPosition;
        //}

        GUILayout.FlexibleSpace();

        //int l_ControlIDBeforeButton = GUIUtility.GetControlID(FocusType.Passive);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Play/Pause"))
        {
            m_movieTexture.pause = !m_movieTexture.pause;
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndArea();

        //if (GUIUtility.hotControl != 0 && CurrentMovieState != MovieState.Seeking)
        //{
        //    if (GUIUtility.hotControl < l_ControlIDBeforeButton)
        //    {
        //        m_MovieStateBeforeSeeking = CurrentMovieState;
        //        CurrentMovieState = MovieState.Seeking;
        //    }
        //}
        //if (GUIUtility.hotControl == 0 && CurrentMovieState == MovieState.Seeking)
        //{
        //    CurrentMovieState = m_MovieStateBeforeSeeking;
        //}
    }

    //void OnGUI()
    //{
    //    if (GUI.Button(new Rect(0, 0, 100, 100), "Seek" ))
    //    {
    //        m_movieTexture.Path = "MovieSamples/loop1.ogv";
    //        m_movieTexture.Play();


            
    //        //m_movieTexture.playPosition = Random.Range(0.0f, (float)m_movieTexture.duration);
    //        //if (m_movieTexture.isPlaying)
    //        //{
    //        //    m_movieTexture.pause = true;
    //        //}
    //        //else
    //        //{
    //        //    if (m_movieTexture.pause)
    //        //    {
    //        //        m_movieTexture.pause = false;
    //        //    }
    //        //    else
    //        //    {
    //        //        m_movieTexture.Play();
    //        //    }
    //        //}
    //    }




    //}
}
