﻿using UnityEngine;
using System.Collections;

public class ScriptMovie : MonoBehaviour {

	public MovieTexture movTexture;
	private int frames = 0;
	private bool pausedAtStart = false;

	// Use this for initialization
	void Start () {
	}

	void Awake () {
		movTexture.Play();
		movTexture.loop = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (frames < 3)
			frames++;

		else
		{
			if(!pausedAtStart)
			{
				pausedAtStart = true;
				movTexture.Pause();
			}
		}
	}
}
