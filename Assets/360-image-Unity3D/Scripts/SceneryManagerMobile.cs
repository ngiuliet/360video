﻿using UnityEngine;
using System.Collections;

public class SceneryManagerMobile : MonoBehaviour {
	
	public GameObject cubeMap;
	public GameObject cylinderMap;
	public GameObject sphereMap;
	private bool GUIVisible = false;
	
	void OnGUI() 
	{
		if (GUI.Button (new Rect (25, Screen.height - 50, 100, 30), "Scenes"))
			GUIVisible = !GUIVisible;
		if (GUIVisible) 
		{
			if (GUI.Button (new Rect (150, Screen.height - 50, 50, 30), "1"))
			{
				cubeMap.SetActive (true);
				sphereMap.SetActive (false);
				cylinderMap.SetActive (false);
			}
			
			if (GUI.Button (new Rect (225, Screen.height - 50, 50, 30), "2"))
			{
				cubeMap.SetActive (false);
				sphereMap.SetActive (true);
				cylinderMap.SetActive (false);
			}
			
			if (GUI.Button (new Rect (300, Screen.height - 50, 50, 30), "3"))
			{
				cubeMap.SetActive (false);
				sphereMap.SetActive (false);
				cylinderMap.SetActive (true);
			}
		}
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
