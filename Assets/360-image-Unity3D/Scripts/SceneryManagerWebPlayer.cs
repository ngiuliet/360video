﻿using UnityEngine;
using System.Collections;

public class SceneryManagerWebPlayer : MonoBehaviour {

	public GameObject cubeMap;
	public GameObject sphereMap;
	public GameObject sphereVideo;

	public Texture2D playTex;

	private int currObject = 0;
	private int numObjects = 3;
	private GameObject[] mapObjects;

	private ScriptMovie scriptMovie;

	private enum FadeState {FadeIn, Opaque, FadeOut, Transparent};
	private FadeState fadeState = FadeState.FadeIn;					// 0 means fade-in, 1 means opaque, 2 means fade-out, 3 means transparent
	private float totalFadeInTime = 3.0f;							// time to fade in expressed in seconds
	private float totalFadeOutTime = 0.5f;							// time to fade in expressed in seconds
	private float fadeTime = 0.0f;
	private float landingAlpha = 0.0f;

	private enum State {Landing, App};
	private State state = State.Landing;							// states are landing screen 0, and application 1
	private bool helpShown = false;

	void OnGUI() 
	{
		if (state == State.Landing) 
		{
			GUI.color = new Color (1, 1, 1, landingAlpha);
			GUI.backgroundColor = new Color (0, 0, 0, 0);

			if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100), playTex))
			{
				if(fadeState == FadeState.Opaque)
					fadeState = FadeState.FadeOut;
			}

			GUI.Label(new Rect(Screen.width - 250, Screen.height - 70, 250, 70), 
			"<size=20><b>Virtual Realities Viewer</b></size>");

			GUI.Label(new Rect(Screen.width - 210, Screen.height - 40, 210, 40), 
			"<size=12>Powered by</size><size=16><b> 3</b></size><size=24><b>Sphere</b></size>");

			if (fadeState == FadeState.Transparent && !scriptMovie.movTexture.isPlaying)
			{
				scriptMovie.movTexture.Play ();
				state = State.App;
				fadeState = FadeState.FadeIn;
			}
		}

		else if(state == State.App)
		{
			GUI.color = new Color (1, 1, 1, landingAlpha);

			GUI.backgroundColor = new Color (0, 0, 0, 0);
			GUI.Button(new Rect(122, Screen.height - 40, 60, 40), "<size=20><b>-</b></size>");

			GUI.backgroundColor = new Color (0, 0, 0, landingAlpha / 2);

			if(GUI.Button(new Rect(10, Screen.height - 40, 112, 40), "<size=20><b>< Previous</b></size>"))
				currObject = (currObject == 0) ? numObjects - 1 : currObject - 1;

			if(GUI.Button(new Rect(182, Screen.height - 40, 85, 40), "<size=20><b>Next ></b></size>"))
				currObject = (currObject == numObjects - 1) ? 0 : currObject + 1;

			if(GUI.Button(new Rect(Screen.width - 210, Screen.height - 40, 200, 40), "<size=20><b>Contact Realtor</b></size>"))
				Application.OpenURL ("http://www.zillow.com");

	
			if(!helpShown)
			{
				if(GUI.Button(new Rect(10, 0, 77, 40), "<size=20><b>Help</b></size>"))
					helpShown = true;
			}
			else
			{
				GUI.Button(new Rect(10, 0, 77, 40), "<size=20><b>Help</b></size>");
				GUI.backgroundColor = new Color (0, 0, 0, 1);
				GUI.Button(new Rect(Screen.width / 2 - 175, Screen.height / 2 - 40, 350, 80), 
				"<size=12>Select <b>Previous</b> or <b>Next</b> to change locations, or\n" +
				"select <b>Contact Realtor</b> to go to the realtor website.</size>");

				if(GUI.Button(new Rect(Screen.width / 2 + 175, Screen.height / 2 - 40, 30, 30), "<size=20><b>X</b></size>"))
					helpShown = false;
			}
		}

		if(!mapObjects[currObject].activeSelf)
			mapObjects[currObject].SetActive(true);

		for(int i = 0; i < numObjects; ++i)
		{
			if(i != currObject)
				mapObjects[i].SetActive(false);
		}
	}

	// Use this for initialization
	void Start () {

		mapObjects = new GameObject[3];

		Instantiate(sphereVideo, new Vector3(0, 0, 0), Quaternion.identity);
		mapObjects[0] = GameObject.Find ("SphereVideo(Clone)");
		scriptMovie = mapObjects[0].GetComponent<ScriptMovie> ();

		Instantiate(cubeMap, new Vector3(0, 0, 0), Quaternion.identity);
		mapObjects[1] = GameObject.Find ("CubeMap(Clone)");
		mapObjects [1].SetActive (false);

		Instantiate(sphereMap, new Vector3(0, 0, 0), Quaternion.identity);
		mapObjects[2] = GameObject.Find ("SphereMap(Clone)");
		mapObjects [2].SetActive (false);


	}
	
	// Update is called once per frame
	void Update () {

		if(fadeState == FadeState.FadeIn)
		{
			if(fadeTime >= totalFadeInTime)
			{
				fadeTime = 0;
				fadeState = FadeState.Opaque;
			}
			else
			{
				landingAlpha = Mathf.Lerp(0, 1, fadeTime / totalFadeInTime); 
				fadeTime += Time.deltaTime;
			}
		}
		else if(fadeState == FadeState.FadeOut)
		{
			if(fadeTime >= totalFadeOutTime)
			{
				fadeTime = 0;
				fadeState = FadeState.Transparent;
				landingAlpha = 0;
			}
			else
			{
				landingAlpha = 1 - Mathf.Lerp(0, 1, fadeTime / totalFadeOutTime); 
				fadeTime += Time.deltaTime;
			}
		}

	}
}
