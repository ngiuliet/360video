﻿// Credit to damien_oconnell from http://forum.unity3d.com/threads/39513-Click-drag-camera-movement
// for using the mouse displacement for calculating the amount of camera movement and panning code.

using UnityEngine;
using System.Collections;


public class OrbitCamera : MonoBehaviour 
{
	//
	// VARIABLES
	//
	
	public float turnSpeed = 60.0f;		// Speed of camera turning when mouse moves in along an axis
	public float panSpeed = 60.0f;		// Speed of the camera when being panned
	public float zoomSpeed = 60.0f;		// Speed of the camera going back and forth
	
	private Vector3 mouseOrigin;	// Position of cursor when mouse dragging starts
	private bool isPanning;		// Is the camera being panned?
	private bool isRotating;	// Is the camera being rotated?
	private bool isZooming;		// Is the camera zooming?
	private bool isOrbiting = true;		// true when the camera is rotating on its own

	private float minimumX = -360F;
	private float maximumX = 360F;
	
	private float minimumY = -89F;
	private float maximumY = 89F;
	
	float rotationY = 0F;

	//
	// UPDATE
	//

	void Update () 
	{

		// Get the left mouse button
		if(Input.GetMouseButtonDown(0))
		{
			// Get mouse origin
			mouseOrigin = Input.mousePosition;
			isRotating = true;
			isOrbiting = false;
		}
		
		// Get the right mouse button
		if(Input.GetMouseButtonDown(1))
		{
			// Get mouse origin
			mouseOrigin = Input.mousePosition;
			isPanning = true;
			isOrbiting = false;
		}
		
		// Get the middle mouse button
		if(Input.GetMouseButtonDown(2))
		{
			// Get mouse origin
			mouseOrigin = Input.mousePosition;
			isZooming = true;
			isOrbiting = false;
		}
		
		// Disable movements on button release
		if (!Input.GetMouseButton(0)) isRotating=false;
		if (!Input.GetMouseButton(1)) isPanning=false;
		if (!Input.GetMouseButton(2)) isZooming=false;

		// Rotate camera along X and Y axis
		if (isRotating)
		{
			Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin) * 10.0f;
			float rotationX = transform.localEulerAngles.y + (pos.x * turnSpeed);
			rotationY += pos.y * turnSpeed;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
			
			//transform.RotateAround(transform.position, transform.right, -pos.y * turnSpeed);
			//transform.RotateAround(transform.position, Vector3.up, pos.x * turnSpeed);
		}
		
		if(isOrbiting)
		{
			Vector3 angles;
			angles.x = 0.0f; angles.y = 0.1f; angles.z = 0.0f;
			transform.Rotate(angles);
		}
		
		// Move the camera on it's XY plane
		if (isPanning)
		{
//			Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
//			
//			Vector3 move = new Vector3(pos.x * panSpeed, pos.y * panSpeed, 0);
//			transform.Translate(move, Space.Self);
		}
		
		// Move the camera linearly along Z axis
		if (isZooming)
		{
//			Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
//			
//			Vector3 move = pos.y * zoomSpeed * transform.forward; 
//			transform.Translate(move, Space.World);
		}
	}
}